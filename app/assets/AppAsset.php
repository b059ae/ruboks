<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/bootstrap-theme.css',
        'css/animate.min.css',
        'css/styles.css',
        'css/responsive.css',
    ];
    public $js = [
        'js/masonry.pkgd.min.js',
        'js/imagesloaded.pkgd.min.js',
        'js/wow.min.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
