<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */
use app\helpers\Html;
use yii\easyii\models\Setting;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Menu;

?>
<footer  id="FOOTER" class="footer">
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="footer-widget">
                        <div class="widget-title footer-wdget-title">
                            <h5 class="text-uppercase">Тренировки</h5>
                        </div>
                        <ul>
                            <li><a href="#">Техника боя</a></li>
                            <li><a href="#">Комбинации ударов</a></li>
                            <li><a href="#">Работа на лапах</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="footer-widget">
                        <div class="widget-title footer-wdget-title">
                            <h5>СЕМИНАРЫ</h5>
                        </div>
                        <ul>
                            <li><a href="#">График семинаров</a></li>
                            <li><a href="#">Условия</a></li>
                            <li><a href="#">Отзывы</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="footer-widget footer-wdget-title">
                        <div class="widget-title">
                            <h5 class="text-uppercase">Мой путь</h5>
                        </div>
                        <ul>
                            <li><a href="#">Обо мне</a></li>
                            <li><a href="#">Мои книги и публикации</a></li>
                            <li><a href="#">Контакты</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="row">
                <div class="col-md-8">
                    <form class="subscribe-form">
                        <input type="email" name="emal" placeholder="Ваш E-mail">
                        <input type="submit" class="btn-common contact-btn" value="ЗАПИСАТЬСЯ НА СЕМИНАР">
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="footer-social subscribe-form">
                        <ul>
                            <li><a class="facebook" href="#"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i></a></li>
                            <li><a class="twitter" href="#"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-sub-middle">
            <div class="row">
                <div class="col-md-4">
                    <div class="address-text">
                        <i class="fa glyphicon glyphicon-map-marker" aria-hidden="true"></i>
                        <p>
                            Россия, Краснодарский край, Сочи
                        </p>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="address-text">
                        <i class="fa glyphicon glyphicon-earphone" aria-hidden="true"></i>
                        <p>
                            8 (967) 644-56-01
                        </p>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="address-text">
                        <i class="fa glyphicon glyphicon-envelope" aria-hidden="true"></i>
                        <p>
                            info@ruboks.ru
                        </p>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="copyright">
                        <p>&copy; <?= date('Y') ?>. <?= Setting::get('title') ?>. Все права защищены.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>