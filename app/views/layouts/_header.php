<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */

use yii\easyii\models\Setting;
use yii\helpers\Url;
use yii\widgets\Menu;
?>
<header id="HOME" class="header-one">
    <div class="">
        <!-- NAVIGATION -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#my-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- TEMPLATE LOGO -->
                    <div class="navbar-brand">
                        <a href="/"><img src="<?= $asset->baseUrl ?>/logo.png" alt="img"><?= Setting::get('title') ?></a>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="my-navbar-collapse">
                    <?= Menu::widget([
                        'items' => yii\easyii\modules\menu\api\Menu::items('main'),
                        'options' => [
                            'class' => 'nav navbar-nav navbar-right',
                        ]
                    ]); ?>
                </div>

            </div>
        </nav>
    </div>
</header>