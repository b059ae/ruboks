<?php
/**
 * Используемый шаблон
 * http://codepassenger.com/html/mountainBike/#
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Menu;

$asset = \app\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <?= $this->render('_header', ['asset' => $asset]); ?>
    <section>
        <?= $content ?>
    </section>
    <?= $this->render('_footer', ['asset' => $asset]); ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>