<?php
$this->title = 'EasyiiCMS start page';
?>

<!-- slider start -->
<div class="slider-wrap">
    <div id="slider_one" class="owl-carousel owl-theme">
        <div class="item">
            <img src="/img/slider-1.jpg" alt="бокс"/>
            <div class="slider-content">
                <div class="container">
                    <h2>Школа бокса<br/>Валерия Щитова</h2>
                    <h3>Эффективная система тренировок</h3>
                    <a href="#" class="btn-common slider-btn text-uppercase">Записаться на семинар</a>
                </div><!-- /.slider-content -->
            </div>
        </div>
    </div>
</div>
<!-- slider end -->
<div class="content-wrapper">
    <div id="ABOUT" class="section-padding about-section">
        <div class="container">
            <div class="section-title-area text-center">
                <h3 class="text-uppercase">Эффективная система тренировок по боксу</h3>
                <span class="glyphicon glyphicon-chevron-down glyphicon-3x"></span>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec <br/> quis lacus convallis, elementum
                    turpis sed, porta urna. felis <br/> vitae justo tempor pulvinar.
                </p>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="about-content wow fadeInUp animated" data-wow-duration="1.5s" data-wow-delay="0s">
                        <div class="icon-circled">
                            <span class="glyphicon glyphicon-star glyphicon-3x"></span>
                        </div>
                        <div class="about-heading">
                            <h5 class="text-uppercase">Персональный тренер</h5>
                        </div>
                        <p>Feel free to stop by the shop for a free estimate prior to service</p>
                    </div><!-- /.about-content -->
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="about-content wow fadeInUp animated" data-wow-duration="1.5s" data-wow-delay="0.5s">
                        <div class="icon-circled">
                            <span class="glyphicon glyphicon-star glyphicon-3x"></span>
                        </div>
                        <div class="about-heading">
                            <h5 class="text-uppercase">Спарринг-партнёр</h5>
                        </div>
                        <p>Feel free to stop by the shop for a free estimate prior to service</p>
                    </div><!-- /.about-content -->
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="about-content wow fadeInUp animated" data-wow-duration="1.5s" data-wow-delay="1s">
                        <div class="icon-circled">
                            <span class="glyphicon glyphicon-star glyphicon-3x"></span>
                        </div>
                        <div class="about-heading">
                            <h5 class="text-uppercase">Подбор техники</h5>
                        </div>
                        <p>Feel free to stop by the shop for a free estimate prior to service</p>
                    </div><!-- /.about-content -->
                </div>
            </div>
        </div>
    </div><!-- /.about-section -->
    <div id="GALLERY" class="gallery-masonry">
        <div class="container">
            <div class="section-title-area text-center">
                <h3 class="text-uppercase">Полный комплекс материалов</h3>
                <span class="glyphicon glyphicon-chevron-down glyphicon-3x"></span>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec <br/> quis lacus convallis, elementum
                    turpis sed, porta urna. felis <br/> vitae justo tempor pulvinar.
                </p>
            </div>
            <div class="filter-button-group gallary-filter-button text-center mrg-b-50">
                <a href="#" class="button">ТЕХНИКА БОЯ</a>
                <a href="#" class="button">ТРЕНИРОВКИ</a>
                <a href="#" class="button">КОМБИНАЦИИ УДАРОВ</a>
                <a href="#" class="button">РАБОТА НА ЛАПАХ</a>
                <a href="#" class="button">ПУБЛИКАЦИИ</a>
                <a href="#" class="button">СЕМИНАРЫ</a>
            </div><!-- /.portfolioFilter -->
        </div>
        <div class="gallary-masonry-layout">
            <div id="gallary_grid">
                <div class="grid-item">
                    <div class="gallary-item-inner ps-rel">
                        <a href="#">
                            <div class="gallery-overlay"></div>
                            <img src="/img/gallery-1.jpg" alt="img" class="img-responsive">
                        </a>
                        <div class="gallary-item-content">
                            <h3>ТЕХНИКА БОЯ</h3>
                        </div>
                    </div>
                </div><!-- /.gallary-item -->
                <div class="grid-item">
                    <div class="gallary-item-inner ps-rel">
                        <a href="#">
                            <div class="gallery-overlay"></div>
                            <img src="/img/gallery-2.jpg" alt="img" class="img-responsive">
                        </a>
                        <div class="gallary-item-content">
                            <h3>ТРЕНИРОВКИ</h3>
                        </div>
                    </div>
                </div><!-- /.gallary-item -->
                <div class="grid-item W2">
                    <div class="gallary-item-inner ps-rel">
                        <a href="#">
                            <div class="gallery-overlay"></div>
                            <img src="/img/gallery-3.jpg" alt="img" class="img-responsive">
                        </a>
                        <div class="gallary-item-content">
                            <h3>КОМБИНАЦИИ УДАРОВ</h3>
                        </div>
                    </div>
                </div><!-- /.gallary-item -->
                <div class="grid-item">
                    <div class="gallary-item-inner ps-rel">
                        <a href="#">
                            <div class="gallery-overlay"></div>
                            <img src="/img/gallery-4.jpg" alt="img" class="img-responsive">
                        </a>
                        <div class="gallary-item-content">
                            <h3>РАБОТА НА ЛАПАХ</h3>
                        </div>
                    </div>
                </div><!-- /.gallary-item -->
                <div class="grid-item">
                    <div class="gallary-item-inner ps-rel">
                        <a href="#">
                            <div class="gallery-overlay"></div>
                            <img src="/img/gallery-5.jpg" alt="img" class="img-responsive">
                        </a>
                        <div class="gallary-item-content">
                            <h3>ПУБЛИКАЦИИ</h3>
                        </div>
                    </div>
                </div><!-- /.gallary-item -->
                <div class="grid-item">
                    <div class="gallary-item-inner ps-rel">
                        <a href="#">
                            <div class="gallery-overlay"></div>
                            <img src="/img/gallery-6.jpg" alt="img" class="img-responsive">
                        </a>
                        <div class="gallary-item-content">
                            <h3>СЕМИНАРЫ</h3>
                        </div>
                    </div>
                </div><!-- /.gallary-item -->
            </div><!-- /.gallary_grid -->
        </div>
    </div><!-- /.gallery-masonry -->
    <div id="BLOG" class="blog-section section-padding">
        <div class="container">
            <div class="section-title-area text-center style-2">
                <h3 class="text-uppercase">Популярные статьи</h3>
                <span class="glyphicon glyphicon-chevron-down glyphicon-3x"></span>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec <br/> quis lacus convallis, elementum
                    turpis sed, porta urna. felis <br/> vitae justo tempor pulvinar.
                </p>
            </div>
            <div class="blog-content-area">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <article class="post-single style-one">
                            <div class="blog-thumb ps-rel">
                                <img src="/img/blog-1.jpg" alt="img" class="img-responsive">
                                <div class="overlay-common"></div>
                                <a class="icon-positiond" href="#"><span class="glyphicon glyphicon-eye-open glyphicon-3x"></span></a>
                            </div>
                            <div class="blog-content">
                                <h5 class="post-title">
                                    <a href="#">
                                        Прямые удары в голову
                                    </a>
                                </h5>
                                <p class="mrg-b-20">
                                    Удары в голову могут иметь решающий перевес на ринге. И еще в уличной драке в целях
                                    самообороны. Расскажем о прямых ударах в голову.
                                </p>
                                <div class="blog-footer">
                                    <div class="post-meta-data">
                                        <span class="post-time">12 сентября 2016</span>

                                    </div>
                                    <div class="post-btn">
                                        <h5 class="more-btn"><a href="#" class="text-uppercase">Читать далее</a></h5>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div><!-- /.blog-content -->
                        </article>
                    </div><!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-6">
                        <article class="post-single style-one">
                            <div class="blog-thumb ps-rel">
                                <img src="/img/blog-2.jpg" alt="img" class="img-responsive">
                                <div class="overlay-common"></div>
                                <a class="icon-positiond" href="#"><span class="glyphicon glyphicon-eye-open glyphicon-3x"></span></a>
                            </div>
                            <div class="blog-content">
                                <h5 class="post-title">
                                    <a href="#">
                                        Что такое перетренированность?
                                    </a>
                                </h5>
                                <p class="mrg-b-20">
                                    Тренировки по боксу — это испытание на выносливость и силу воли. Как грамотно
                                    восстанавливать свои силы. Как не сойти с дистанции и оставаться в боксерском строю.
                                </p>
                                <div class="blog-footer">
                                    <div class="post-meta-data">
                                        <span class="post-time">11 декабря 2016</span>

                                    </div>
                                    <div class="post-btn">
                                        <h5 class="more-btn"><a href="#" class="text-uppercase">Читать далее</a></h5>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div><!-- /.blog-content -->
                        </article>
                    </div><!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-6">
                        <article class="post-single style-one last">
                            <div class="blog-thumb ps-rel">
                                <img src="/img/blog-3.png" alt="img" class="img-responsive">
                                <div class="overlay-common"></div>
                                <a class="icon-positiond" href="#"><span class="glyphicon glyphicon-eye-open glyphicon-3x"></span></a>
                            </div>
                            <div class="blog-content">
                                <h5 class="post-title">
                                    <a href="#">
                                        Для чего нужна пневмогруша?
                                    </a>
                                </h5>
                                <p class="mrg-b-20">
                                    Этот вопрос нам задают часто. Вообще-то понятно, что она для чего-то нужна, но тогда
                                    возникают уже следующие вопросы: как с ней работать и что она развивает.
                                </p>
                                <div class="blog-footer">
                                    <div class="post-meta-data">
                                        <span class="post-time">22 декабря 2016</span>
                                    </div>
                                    <div class="post-btn">
                                        <h5 class="more-btn"><a href="#" class="text-uppercase">Читать далее</a></h5>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div><!-- /.blog-content -->
                        </article>
                    </div><!-- /.col-md-4 -->
                </div>
            </div>
        </div>
    </div><!-- /.blog-section -->
</div>
